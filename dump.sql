CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Smartphones'),
(2, 'Tablets');


CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `options` (`id`, `name`, `product_id`) VALUES
(1, 'Compact size', 1),
(2, 'Used', 2),
(3, 'Good for reading', 2);


CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `product` (`id`, `name`, `description`, `price`, `category_id`) VALUES
(1, 'Iphone', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed ex pharetra, rhoncus tortor sit amet, egestas diam. Praesent nec ligula ac diam lobortis rhoncus.', '102.99', 1),
(2, 'Ipad', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed ex pharetra, rhoncus tortor sit amet, egestas diam. Praesent nec ligula ac diam lobortis rhoncus.', '220.00', 2),
(3, 'Samsung', 'Galaxy S10', '100.00', 1);


ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);


ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;


ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;
