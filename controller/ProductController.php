<?php

namespace Controller;

use Core\Controller;
use Model\Category;
use Model\Option;
use Model\Product;

/**
 * Class ProductController
 * @package Controller
 */
class ProductController extends Controller
{
    public function index()
    {
        $model = new Product();
        $products = $model->listProductWithCategory();
        $this->render('product/index', [
            'products' => $products
        ]);
    }

    public function products()
    {
        $model = new Product();
        $products = $model->listProductWithCategory();
        $this->renderOnly('product/list', [
            'products' => $products
        ]);
    }


    public function add()
    {
        $category_model = new Category();
        $categories = $category_model->selectAll();
        $this->renderOnly('product/new', ['categories' => $categories]);
    }

    public function create()
    {
        if ($_POST) {
            $model = new Product();
            $model->setName(htmlspecialchars($_POST['name']));
            $model->setDescription(htmlspecialchars($_POST['description']));
            $model->setPrice((float)$_POST['price']);
            $model->setCategoryId((int)$_POST['category']);
            if ($model->save()) {
                echo "Saved successfully";
            } else {
                echo "Cannot save";
            }


        }
    }

    public function edit($id)
    {

        $model = new Product();
        $model->setId((int)$id);
        $product = $model->selectOne();
        $category_model = new Category();
        $categories = $category_model->selectAll();
        $option_model = new Option();
        $options = $option_model->listOptionsByProductId((int)$id);

        $this->renderOnly('product/edit', [
            'product' => $product,
            'categories' => $categories,
            'options' => $options,
        ]);
    }

    public function update($id)
    {
        if ($_POST) {
            $model = new Product();
            $model->setId((int)$id);
            $model->setName(htmlspecialchars($_POST['name']));
            $model->setDescription(htmlspecialchars($_POST['description']));
            $model->setPrice((float)$_POST['price']);
            $model->setCategoryId((int)$_POST['category']);
            if ($model->save()) {
                $this->setOptions($_POST['options'], $id);
                echo "Saved successfully";
            } else {
                echo "Cannot save";
            }


        }
    }

    protected function setOptions($options, $productId)
    {
        $model = new Option();
        $model->deleteAllOptionsByProductId($productId);

        foreach ($options as $option) {
            if ($option) {
                $model->setName(htmlspecialchars($option));
                $model->setProductId((int)$productId);
                $model->save();
            }

        }

    }

    public function delete($id)
    {
        $model = new Product();
        $model->setId((int)$id);
        if ($model->delete()) {
            echo "Delete successfully";
        } else {
            echo "Cannot delete";
        }
    }
}