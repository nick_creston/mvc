<?php

namespace Core;

/**
 * Class Controller
 * @package Core
 */
class Controller
{
    /**
     * Render in layout template
     * @param $template
     * @param null $params
     */
    public function render($template, $params=null) {
        ob_start();
        if(is_array($params)) {
            extract($params);
        }
        require 'views/'.$template.'.php';
        $content = ob_get_clean();
        require 'views/layout.php';
    }

    /**
     * Render standalone template
     * @param $template
     * @param null $params
     */
    public function renderOnly($template, $params=null) {
        ob_start();
        if(is_array($params)) {
            extract($params);
        }
        require 'views/'.$template.'.php';
    }

    public function renderJson($params) {
        echo json_encode($params);
    }
}