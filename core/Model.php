<?php

namespace Core;

/**
 * Class Model
 * @package Core
 */
class Model
{
    protected $tableName;
    protected $primaryKey;
    private $columns=[];

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed
     * @throws \ErrorException
     */
    public function __call($name, $arguments)
    {
        $methodPrefix = substr($name, 0, 3);
        $key = strtolower(substr($name, 3));
        if(strlen($key) > 2 && substr($key, -2) === 'id') {
            $key = substr($key, 0, -2). '_id';
        }

        if($methodPrefix == 'set' && count($arguments) == 1)
        {
            $value = $arguments[0];
            $this->columns[$key] = $value;
        }
        elseif($methodPrefix == 'get')
        {
            if(array_key_exists($key, $this->columns)) return $this->columns[$key];
        }
        else {
            throw new \ErrorException('method not exist');
        }
    }

    /**
     * @return bool
     */
    public function save() {
        $query =  "REPLACE INTO " . $this->tableName . " (" . implode(",", array_keys($this->columns)) . ") VALUES(";
        $keys = [];
        $values = [];
        foreach ($this->columns as $key => $value) {
            $keys[":".$key] = $value;
            $values[$key] = $value;
        }
        $query .= implode(",", array_keys($keys)).")";
        return DataBase::run($query, $keys);
    }

    /**
     * @param array $condition
     * @return array
     */
    public function selectAll($condition=[]) {
        $query = "SELECT * FROM " . $this->tableName;
        if(!empty($condition)){
            $query .= " WHERE ";
            foreach ($condition as $key => $value) {
                $query .= $key . "=:".$key." AND ";
            }
        }
        return DataBase::run($query, $condition)->fetchAll(\PDO::FETCH_ASSOC);

    }

    /**
     * @return mixed
     */
    public function selectOne() {
        $query = "SELECT * FROM " . $this->tableName. " WHERE id=:id";
        return DataBase::run($query, [':id' => $this->columns[$this->primaryKey]])->fetch();
    }

    /**
     * @return bool|false|\PDOStatement
     */
    function delete(){
        $query = "DELETE FROM " . $this->tableName . " WHERE ".$this->primaryKey."=:id";
        return DataBase::run($query, [':id' => $this->columns[$this->primaryKey]]);
    }
}