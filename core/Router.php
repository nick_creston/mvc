<?php

namespace Core;

/**
 * Simple router
 * @package Core
 */
class Router
{
    public $routes = [];
    public $handler;
    public $params;

    public function __construct($routes)
    {
        $this->routes = array_merge($routes);
    }

    public function match()
    {
        $uri = reset(explode('?', $_SERVER["REQUEST_URI"]));
        $requestedUrl = urldecode(rtrim($uri, '/'));

        /**
         * default
         */
        if (strlen($requestedUrl) == 0) {
            $requestedUrl = '/';
        }

        /**
         * if uri is equals to route
         */
        if (isset($this->routes[$requestedUrl])) {
            $this->handler = $this->routes[$requestedUrl];
            return true;
        }

        foreach ($this->routes as $route => $handler) {
            if (strpos($route, ':') !== false) {
                $route = str_replace(':id', '([0-9]+)', $route);
            }
            if (preg_match('#^' . $route . '$#', $requestedUrl, $matches)) {
                $this->handler = $handler;
                $this->params = $matches[1];
                return true;
            }
        }
        return false;
    }

    public function run()
    {
        $ca = explode('#', $this->handler);
        $controllerName = $ca[0];
        $action = $ca[1];

        $controller = new $controllerName();

        return call_user_func(array($controller, $action), $this->params);
    }
}