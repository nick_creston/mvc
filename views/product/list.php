<?php $productHelper = new \Helper\ProductHelper(); ?>
<table id="tbl" class="table table-bordered table-striped mt-3">
    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Category</th>
        <th scope="col">Options</th>
        <th scope="col">Price</th>
        <th scope="col">-</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($products as $product): ?>
        <tr id="product_<?php echo $product['id'] ?>">
            <td><?php echo $product['id'] ?></td>
            <td><?php echo $product['name'] ?></td>
            <td><?php echo $product['category'] ?></td>
            <td><?php echo $productHelper->getProductOptions($product['id']) ?>

            </td>
            <td><?php echo $product['price'] ?></td>
            <td>
                <Button class="btn btn-primary edit" data-toggle="modal" data-target="#modal">Edit</Button>
                <Button class="btn btn-danger delete" data-toggle="modal" data-target="#modal">Delete</Button>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>