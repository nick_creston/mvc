<form id="form">
    <input type="hidden" id="id" value=""/>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="" required>
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="category" required>
            <?php foreach ($categories as $category) : ?>
                <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" rows="3" placeholder="Enter description"></textarea>
    </div>

    <div class="form-group">
        <label for="name">Price</label>
        <input type="text" class="form-control" id="price" name="price" placeholder="Enter price" value="">
    </div>


    <button type="button" class="btn btn-primary product-add">Submit</button>
</form>