<form id="form">
    <input type="hidden" id="id" value="<?php echo $product['id']?>"/>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="<?php echo $product['name']?>" required >
    </div>
    <div class="form-group">
        <label for="category">Category</label>
        <select class="form-control" id="category" name="category" required>
            <?php foreach ($categories as $category) :?>
            <option value="<?php echo $category['id']?>" <?php if($product['category_id'] == $category['id']) echo 'selected'?>><?php echo $category['name']?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" rows="3" placeholder="Enter description"><?php echo $product['description']?></textarea>
    </div>

    <div class="form-group">
        <label for="name">Price</label>
        <input type="text" class="form-control" id="price" name="price" placeholder="Enter price" value="<?php echo $product['price']?>">
    </div>

    <div class="form-group">
        <p>Product options</p>
        <button type="button" class="btn btn-sm btn-primary mb-2 add-option">Add</button>
        <div class="sizes">
            <?php foreach ($options as $option) :?>
                <div class="form-row mb-3">
                    <div class="col-6">
                        <input type="text" class="form-control size" value="<?php echo $option['name']?>"/>
                    </div>
                    <div class="col-3">
                        <button type="button" class="btn btn-danger ml-2 del-size">X</button>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>


    <button type="button" class="btn btn-primary product-edit">Submit</button>
</form>