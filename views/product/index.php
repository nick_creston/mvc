<?php $productHelper = new \Helper\ProductHelper(); ?>
<h1>Test app</h1>

<Button class="btn btn-primary add mt-3" data-toggle="modal" data-target="#modal">Add product</Button>

<div class="product-list">

</div>


<div id="modal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
