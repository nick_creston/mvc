<?php

namespace Helper;

use Model\Option;

class ProductHelper
{
    /**
     * Get Product options
     * @param $productId
     * @return string
     */
    public function getProductOptions($productId)
    {
        $model = new Option();
        $options = $model->listOptionsByProductId($productId);
        $result = [];
        foreach ($options as $option) {
            $result[] = $option['name'];
        }
        return implode(', ', $result);
    }
}