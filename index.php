<?php
require "vendor/autoload.php";
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$routes = [
    '/'=> '\Controller\ProductController#index',
    '/products/list'=> '\Controller\ProductController#products',
    '/products/:id' => '\Controller\ProductController#show',
    '/products/add' => '\Controller\ProductController#add',
    '/products/create' => '\Controller\ProductController#create',
    '/products/edit/:id' => '\Controller\ProductController#edit',
    '/products/update/:id' => '\Controller\ProductController#update',
    '/products/delete/:id'=>'\Controller\ProductController#delete'
];

$router = new \Core\Router($routes);

if($router->match()) {
    $router->run();
}
else {
   echo 'NOT FOUND';
}