<?php

namespace Model;

use Core\DataBase;
use Core\Model;

/**
 * Class Product
 * @package Model
 */
class Product extends Model
{
    protected $tableName = 'product';
    protected $primaryKey = 'id';

    protected $name;
    protected $description;
    protected $price;
    protected $categoryId;

    /**
     * @return array
     */
    public function listProductWithCategory() {
        $query = "SELECT p.id, p.name, p.price, p.category_id, c.name as category FROM ".$this->tableName." AS p LEFT JOIN category as c ON p.category_id=c.id";
        return DataBase::run($query)->fetchAll(\PDO::FETCH_ASSOC);
    }

}