<?php


namespace Model;

use Core\Model;

/**
 * Class Category
 * @package Model
 */
class Category extends Model
{
    protected $tableName = 'category';
    protected $primaryKey = 'id';

    protected $name;
}