<?php

namespace Model;

use Core\DataBase;
use Core\Model;

/**
 * Product options
 * Class Option
 * @package Model
 */
class Option extends Model
{
    protected $tableName = 'options';
    protected $primaryKey = 'id';

    protected $name;
    protected $product_id;

    /**
     * @param integer $productId
     * @return array
     */
    public function listOptionsByProductId($productId)
    {
        $query = "SELECT * FROM " . $this->tableName . " WHERE product_id=:product_id";
        return DataBase::run($query, [':product_id' => $productId])->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $productId
     * @return bool|false|\PDOStatement
     */
    public function deleteAllOptionsByProductId($productId)
    {
        $query = "DELETE FROM " . $this->tableName . " WHERE product_id=:product_id";
        return DataBase::run($query, [':product_id' => $productId]);
    }
}