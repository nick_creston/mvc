$(function () {
  listTable();

  $("#tbl").tablesorter({
    headers: {5: {sorter: false, filter: false}},
    theme: "bootstrap",
    widthFixed: true,
    widgets: ["filter", "columns", "zebra"],
    widgetOptions: {
      zebra: ["even", "odd"],
      columns: ["primary", "secondary", "tertiary"],
      filter_cssFilter: [
        'form-control',
        'form-control',
        'form-control',
        'form-control',
        'form-control',
        'form-control'
      ]
    }

  });

  $('body').on('click', '.edit', function () {
    let string_id = $(this).parent().parent().attr('id');
    let id = string_id.split('_')[1];
    $.ajax({
      type: 'post',
      url: `/products/edit/${id}`,
      dataType: 'html',
      success: function (data) {
        $('.modal-body').html(data);
      }
    })
  });

  $('.add').click(function () {
    $.ajax({
      type: 'get',
      url: '/products/add',
      dataType: 'html',
      success: function (data) {
        $('.modal-body').html(data);
      }
    })
  });

  $('.delete').click(function () {
    let string_id = $(this).parent().parent().attr('id');
    let $this = $(this);
    let id = string_id.split('_')[1];
    $.ajax({
      type: 'post',
      url: `/products/delete/${id}`,
      dataType: 'html',
      success: function (data) {
        $('.modal-body').html(data);
        $this.parent().parent().hide()
      }
    })
  });

  $('body').on('click', '.product-edit', function () {
    let id = $('#id').val();
    let name = $("#name").val();
    let description = $("#description").val();
    let price = $("#price").val();
    let category = $("#category").val();
    let options = [];
    $('.option').each(function () {
      options.push($(this).val())
    });


    let form = $('#form')
    form.validate({
      rules: {
        price: {
          required: true,
          number: true
        }
      }
    });

    if (form.valid()) {
      $.ajax({
        type: 'post',
        url: `/products/update/${id}`,
        dataType: 'html',
        data: {name: name, description: description, price: price, category: category, options: options},
        success: function (data) {
          listTable();
          $('.modal-body').html(data);
        }
      })
    }


  });

  $('body').on('click', '.product-add', function () {
    let name = $("#name").val();
    let description = $("#description").val();
    let price = $("#price").val();
    let category = $("#category").val();

    let form = $('#form')
    form.validate({
      rules: {
        price: {
          required: true,
          number: true
        }
      }
    });
    if (form.valid()) {
      $.ajax({
        type: 'post',
        url: '/products/create/',
        dataType: 'html',
        data: {name: name, description: description, price: price, category: category},
        success: function (data) {
          listTable();
          $('.modal-body').html(data);
        }
      })
    }
  })

  $('body').on('click', '.add-option', function () {
    let html = '<div class="form-row mb-3">' +
      '<div class="col-6">' +
      '<input type="text" class="form-control option"/>' +
      '</div>' +
      '<div class="col-3">' +
      '<button type="button" class="btn btn-danger ml-2 del-option">X</button>' +
      '</div></div>';
    $('.modal-body').find('div.sizes').append(html)
  })

  $('body').on('click', '.del-option', function () {
    $(this).parent().parent().remove();
  })

});

/**
 * Load product list
 */
function listTable() {
  $.get("/products/list", function (data) {
    $(".product-list").html(data);
    // $("#tbl").tablesorter().trigger('update');
  });

}

$(document).ajaxStop(function () {
  $("#tbl").tablesorter({
    headers: {5: {sorter: false, filter: false}},
    theme: "bootstrap",
    widthFixed: true,
    widgets: ["filter", "columns", "zebra"],
    widgetOptions: {
      zebra: ["even", "odd"],
      columns: ["primary", "secondary", "tertiary"],
      filter_cssFilter: [
        'form-control',
        'form-control',
        'form-control',
        'form-control',
        'form-control',
        'form-control'
      ]
    }

  });
})