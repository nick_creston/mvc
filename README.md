**Installation**

1. Create database.
2. Import _dump.sql_ to database.
3. Run `php composer.phar install` for generate autoload.
4. Create `.env` file by `.env.example`